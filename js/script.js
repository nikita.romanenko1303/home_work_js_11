const error = document.querySelector(".before_error");
const passwordForm = document.querySelector(".password-form");

getFirstEye(passwordForm).addEventListener("click", event => {
    if (event.target.classList.contains("fa-eye")) {
       event.target.classList.remove("fa-eye");
       event.target.classList.add("fa-eye-slash");
       getFirstInput(passwordForm).setAttribute("type", "text")
   } else {
       event.target.classList.remove("fa-eye-slash");
       event.target.classList.add("fa-eye");
        getFirstInput(passwordForm).setAttribute("type", "password")
   }
})
 getSecondEye(passwordForm).addEventListener("click", event => {
     if (event.target.classList.contains("fa-eye")) {
         event.target.classList.remove("fa-eye");
         event.target.classList.add("fa-eye-slash");
         getSecondInput(passwordForm).setAttribute("type", "text")
     } else {
         event.target.classList.remove("fa-eye-slash");
         event.target.classList.add("fa-eye");
         getSecondInput(passwordForm).setAttribute("type", "password")
     }
 })

getButton(passwordForm).addEventListener("click", event => {
    if (getFirstInput(passwordForm).value === getSecondInput(passwordForm).value) {
        alert("You are welcome");
        if (error.classList.contains("error")) {
            error.classList.add("before_error");
            error.classList.remove("error");
        }
    } else {
        error.classList.add("error");
        error.classList.remove("before_error");
    }
})

function getFirstEye(passwordForm) {
    return passwordForm.children[0].children[1];
}
function getSecondEye(passwordForm) {
    return passwordForm.children[1].children[2];
}
function getFirstInput(passwordForm) {
    return passwordForm.children[0].children[0];
}
function getSecondInput(passwordForm) {

    return passwordForm.children[1].children[1];
}
function getButton(passwordForm){
    return passwordForm.children[2];
}